package lab4.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by Ju1ce on 6/2/2016.
 */

@RunWith(Cucumber.class)

@CucumberOptions (
        plugin = {"html:target/cucumber-report/tests", "json:target/cucumber.json"},
        features = "src/test/java/lab4/features",
        glue = "lab4/steps",
        tags = "@fullset") //@fullset @rozetka @stack @couple

public class Runner {
    public static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();

    }

    @AfterClass
    public static void tearDown() {
        driver.close();
    }

//    @Before
//    public void setUp() throws Exception {
//        driver = new FirefoxDriver();
//        baseUrl = "http://rozetka.com.ua";
//        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//        driver.get(baseUrl);
//        driver.manage().window().maximize();
//        startPage = new StartPage(driver);
//    }
//
//    @After
//    public void afterMethod() {
//        driver.close();
//    }

}
