@stack @fullset
Feature: test Stack page

  Background:
    Given I open Stackoverflow start page

  Scenario: 001_check featured count is more that 300
#    When I open Stackoverflow start page
    Then I see featured count is more that 300

  Scenario: 002_check Sing Up page has Google, Facebook buttons
#    Given I open Stackoverflow start page
    When I navigate to Sign Up page
    Then The buttons Google and Facebook are present

  @couple
  Scenario: 003_check any top question has been raised today
#    Given I open Stackoverflow start page
    When I click the first question
    Then I see "today" element

  @couple
  Scenario: 004_check there is present a job offer for $100K on a Stat page within the Job Offers sectoin
#    When I open Stackoverflow start page
    Then I see a job offer for 100K

