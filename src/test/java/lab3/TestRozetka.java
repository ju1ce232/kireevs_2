package lab3;

import lab2_3.pages.rozetka.Basket;
import lab2_3.pages.rozetka.ChooseYourCityPopup;
import lab2_3.pages.rozetka.StartPage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by juice on 28.05.2016.
 */
public class TestRozetka {
    private WebDriver driver;
    private String baseUrl;
    protected StartPage startPage;



    @Before
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
        baseUrl = "http://rozetka.com.ua";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(baseUrl);
        driver.manage().window().maximize();
        startPage = new StartPage(driver);
    }

    @After
    public void tearDown() {
        driver.close();
    }

    @Test
    public void testLogo() throws Exception {
        assertTrue("Can't find element 'Logo'",
                startPage.img_startpage_logo.isDisplayed());
    }

    @Test
    public void testMenuApple() throws Exception {
        assertTrue("Can't find element 'Apple' button",
                startPage.btn_startpage_apple.isDisplayed());
    }

    @Test
    public void testMenuContainsMP3() throws Exception {
        assertTrue("Can't find element of menu which contains 'MP3'",
                startPage.btn_startpage_containsMP3.isDisplayed());
    }

    @Test
    public void testCityChooserPopup() throws Exception {
        ChooseYourCityPopup chooseYourCityPopup = startPage.clickOnCityChooser();

        boolean allCities = false;
        if (chooseYourCityPopup.lnk_cityChooserPopup_kharkov.isDisplayed()
                && chooseYourCityPopup.lnk_cityChooserPopup_kiev.isDisplayed()
                && chooseYourCityPopup.lnk_cityChooserPopup_odessa.isDisplayed()) {
            allCities = true;
        }
        assertTrue("Can't find one of elements 'Kharkov, Kiev or Odessa'", allCities);
    }

    @Test
    public void testBasketisEmpty() throws Exception {
        Basket basket = startPage.clickOnBasketBtn();
        assertTrue("The basket is not empty",
                basket.elem_basketPage_basketIsEmpty.isDisplayed());
    }
}
