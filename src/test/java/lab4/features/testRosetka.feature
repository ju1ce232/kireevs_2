@rozetka @fullset
Feature: test Rozetka page

  Background:
    Given I open Rozetka start page

  @couple
  Scenario: 001_check Rozetka logo
#    When I open Rozetka start page
    Then Rozetka logo is present at the left top corner

  Scenario: 002_check Apple section of Catalog menu
#    When I open Rozetka start page
    Then Apple section is present within the Catalog menu

  @couple
  Scenario: 003_check that Catalog menu contains a section with "MP3" text
#    When I open Rozetka start page
    Then Catalog menu contains a section with "MP3" text

  Scenario: 004_check that "Выберите город" popup contains “Харьков”, “Одесса”, “Киев” items
#    Given I open Rozetka start page
    When I click on Choose Your City link
    Then “Харьков”, “Одесса”, “Киев” items are present

  Scenario: 005_check that basket is empty
#    Given I open Rozetka start page
    When I click basket link
    Then The Basket page is empty
