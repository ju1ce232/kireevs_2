package lab2_3.pages.rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by juice on 29.05.2016.
 */
public class Basket {
    private WebDriver driver;

    public Basket(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "//*[@id='drop-block']/h2[contains(text(), 'Корзина пуста')]")
    public WebElement elem_basketPage_basketIsEmpty;


}
