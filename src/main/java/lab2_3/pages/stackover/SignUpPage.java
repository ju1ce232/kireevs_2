package lab2_3.pages.stackover;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by juice on 29.05.2016.
 */
public class SignUpPage {
    private WebDriver driver;

    public SignUpPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "//div[@data-provider='google']")
    public WebElement btn_signUpPage_googleBtn;

    @FindBy(xpath = "//div[@data-provider='facebook']")
    public WebElement btn_signUpPage_facebookBtn;

}
