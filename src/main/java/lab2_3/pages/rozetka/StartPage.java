package lab2_3.pages.rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by student on 5/26/2016.
 */
public class StartPage {
    private WebDriver driver;

    public StartPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "//div[@class='logo']/img")
    public WebElement img_startpage_logo;

    @FindBy(xpath = "//*[@id='m-main']//a[@href='http://rozetka.com.ua/apple/c4627486/']")
    public WebElement btn_startpage_apple;

    @FindBy (xpath = "//*[@id='m-main']/li/a[contains(text(), 'MP3')]")
    public WebElement btn_startpage_containsMP3;

    @FindBy (xpath = ".//*[@id='city-chooser']/a")
    public WebElement btn_startpage_city;

    @FindBy (xpath = ".//*[@id='body-header']//*[contains(text(), 'Корзина')]")
    public WebElement btn_startpage_basket;

    public ChooseYourCityPopup clickOnCityChooser (){
        btn_startpage_city.click();
        return new ChooseYourCityPopup(driver);
    }

    public Basket clickOnBasketBtn (){
        btn_startpage_basket.click();
        return new Basket(driver);
    }
}
