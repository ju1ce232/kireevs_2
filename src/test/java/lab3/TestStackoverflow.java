package lab3;

import lab2_3.pages.stackover.SignUpPage;
import lab2_3.pages.stackover.StartPage;
import lab2_3.pages.stackover.QuestionPage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;


/**
 * Created by juice on 28.05.2016.
 */
public class TestStackoverflow {
    private WebDriver driver;
    private String baseUrl;
    protected StartPage startPage;

    @Before
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
        baseUrl = "https://stackoverflow.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(baseUrl);
        driver.manage().window().maximize();
        startPage = new StartPage(driver);
    }

    @After
    public void tearDown() {
        driver.close();
    }

    @Test
    public void testBountyIndicator() throws Exception {
        String count = startPage.elem_startpage_bountyIndicator.getText();
        int countFeatured = Integer.parseInt(count);
        assertTrue("Featured count is equals or less then 300",
                countFeatured > 300);
    }

    @Test
    public void testSignUpBtns() throws Exception {
        SignUpPage signUpPage = startPage.clickSignUp();
        boolean isBtnsExist = false;
        if (signUpPage.btn_signUpPage_facebookBtn.isDisplayed()
                && signUpPage.btn_signUpPage_googleBtn.isDisplayed()) {
            isBtnsExist = true;
        }
        assertTrue("Can't find one of the buttons 'Facebook or Google'", isBtnsExist);
    }

    @Test
    public void testQuestion() throws Exception {
        QuestionPage questionPage = startPage.clickFirstQuestion();

        String citeDate = questionPage.elem_questionPage_today.getAttribute("title");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss'Z'");

        Date dateActual = sdf.parse(citeDate);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateActual);

        Date dateToday = new Date();
        Calendar calendar2 = Calendar.getInstance();
        calendar.setTime(dateToday);

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar2.set(Calendar.HOUR_OF_DAY, 0);
        calendar2.set(Calendar.MINUTE, 0);
        calendar2.set(Calendar.SECOND, 0);
        calendar2.set(Calendar.MILLISECOND, 0);

        assertTrue("The date of the Question is not today's date", calendar.equals(calendar2));
    }

    @Test
    public void testJobOffers() throws Exception {
        List<String> listForTextsTakenFromWebElements = new ArrayList<String>(); //лист для хранения всех text() значений наших вебэлементов
        List<String> listWithSplittedOffers = new ArrayList<String>(); //лист с распарсенными значениями text()
        List<String> listWithParsedStringOffers = new ArrayList<String>(); //финальный лист со строчными офферами
        List<Integer> listWithFinalOffers = new ArrayList<Integer>(); //финальный лист с интовыми офферами

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        try {
            for (WebElement aListTest : startPage.elem_startpage_jobOfferBoxTitle) {
                listForTextsTakenFromWebElements.add(aListTest.getText()); //залили все text() значения в лист
            }
            for (String aListTestStr : listForTextsTakenFromWebElements) {
                String[] temp = aListTestStr.split(" - "); //разбил каждый элемент листа по регулярке
                Collections.addAll(listWithSplittedOffers, temp); //добавил все элементы массива в лист
            }

            for (String a : listWithSplittedOffers) {
                if (a.contains("$")) {
                    listWithParsedStringOffers.add(a); //добавил только элементы с "$" в новый лист
                }
            }
            if (listWithParsedStringOffers.isEmpty()) { //если лист пустой
                throw new Exception(); // то кинул исключение
            }
            for (int i = 0; i < listWithParsedStringOffers.size(); i++) {
                listWithParsedStringOffers.set(i, listWithParsedStringOffers.get(i).replace("$", "")); //преобразовываем
                listWithParsedStringOffers.set(i, listWithParsedStringOffers.get(i).replace(" ", "")); //к
                listWithParsedStringOffers.set(i, listWithParsedStringOffers.get(i).replace("K", "")); //нужному
                listWithParsedStringOffers.set(i, listWithParsedStringOffers.get(i).replace("k", "")); //виду
                listWithParsedStringOffers.set(i, listWithParsedStringOffers.get(i).concat("000")); //строки
                listWithFinalOffers.add(Integer.parseInt(listWithParsedStringOffers.get(i))); // и перегоняем в инт
            }

            boolean offerIsPresent = false;//делаем флаг
            int count = 0; //счетчик для сообщения
            for (Integer i : listWithFinalOffers) { //берем каждый элемент листа
                if (i >= 100000) { //и проверяем что больше либо равен 100к
                    offerIsPresent = true; //офферы есть!
                    count++;
                }
            }
            if (offerIsPresent) {
                System.out.println(count + " - 100K offer(s) found");
            }
            assertTrue("No 100K offers!!!", offerIsPresent);
        } catch (Exception e) { //лист пустой
            fail("There is no job offers! Go away!!!");
        }
    }
}
