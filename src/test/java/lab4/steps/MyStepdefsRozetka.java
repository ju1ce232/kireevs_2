package lab4.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lab2_3.pages.rozetka.Basket;
import lab2_3.pages.rozetka.ChooseYourCityPopup;
import lab2_3.pages.rozetka.StartPage;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;
import static lab4.runner.Runner.*;

/**
 * Created by Ju1ce on 6/3/2016.
 */
public class MyStepdefsRozetka {
    private String baseUrl = "http://rozetka.com.ua";
    public StartPage startPage;
    private static ChooseYourCityPopup chooseYourCityPopup;
    private static Basket basket;

    @Given("^I open Rozetka start page$")
    public void iOpenRozetkaStartPage() throws Throwable {
        driver.get(baseUrl);
        startPage = new StartPage(driver);
    }

    @Then("^Rozetka logo is present at the left top corner$")
    public void rozetkaLogoIsPresentAtTheLeftTopCorner() throws Throwable {
        assertTrue("Can't find element 'Logo'",
                startPage.img_startpage_logo.isDisplayed());
    }

    @Then("^Apple section is present within the Catalog menu$")
    public void appleSectionIsPresentWithinTheCatalogMenu() throws Throwable {
        assertTrue("Can't find element 'Apple' button",
                startPage.btn_startpage_apple.isDisplayed());
    }

    @Then("^Catalog menu contains a section with \"([^\"]*)\" text$")
    public void catalogMenuContainsASectionWithText(String arg0) throws Throwable {
        assertTrue("Can't find element of menu which contains 'MP3'",
                startPage.btn_startpage_containsMP3.isDisplayed());
    }

    @When("^I click on Choose Your City link$")
    public void iClickOnChooseYourCityLink() throws Throwable {
        chooseYourCityPopup = startPage.clickOnCityChooser();
    }

    @Then("^“Харьков”, “Одесса”, “Киев” items are present$")
    public void kharkovOdessKievItemsArePresent() throws Throwable {
        boolean allCities = false;
        if (chooseYourCityPopup.lnk_cityChooserPopup_kharkov.isDisplayed()
                && chooseYourCityPopup.lnk_cityChooserPopup_kiev.isDisplayed()
                && chooseYourCityPopup.lnk_cityChooserPopup_odessa.isDisplayed()) {
            allCities = true;
        }
        assertTrue("Can't find one of elements 'Kharkov, Kiev or Odessa'", allCities);
    }

    @When("^I click basket link$")
    public void iClickBasketLink() throws Throwable {
        basket = startPage.clickOnBasketBtn();
    }

    @Then("^The Basket page is empty$")
    public void theBasketPageIsEmpty() throws Throwable {
        assertTrue("The basket is not empty",
                basket.elem_basketPage_basketIsEmpty.isDisplayed());
    }
}
