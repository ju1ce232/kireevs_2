package lab2_3.pages.stackover;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by juice on 29.05.2016.
 */
public class StartPage {
    private WebDriver driver;

    public StartPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "//*[@id='tabs']//span[@class='bounty-indicator-tab']")
    public WebElement elem_startpage_bountyIndicator;

    @FindBy(xpath = "//a[contains(text(), 'sign up')]")
    public WebElement lnk_startpage_signUp;

    @FindBy(xpath = ".//*[@id='question-mini-list']/div[1]/div[2]/h3/a")
    public WebElement lnk_startpage_firstQuestion;

    //    @FindBy(xpath = ".//div[@class='job-info']/div[@class='title']")
    @FindBy(xpath = ".//*[@id='hireme']/div/div/div[1]/div[1] | .//*[@id='hireme']/div/ul/li/a/div[1]")
    public List<WebElement> elem_startpage_jobOfferBoxTitle; //собрал все вебэлементы в лист

    public SignUpPage clickSignUp() {
        lnk_startpage_signUp.click();
        return new SignUpPage(driver);
    }

    public QuestionPage clickFirstQuestion() {
        lnk_startpage_firstQuestion.click();
        return new QuestionPage(driver);
    }
}
