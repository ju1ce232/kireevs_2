package lab2_3.pages.rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by juice on 29.05.2016.
 */
public class ChooseYourCityPopup {
    private WebDriver driver;

    public ChooseYourCityPopup(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "//*[@locality_id='1'][contains(text(), 'Киев')]")
    public WebElement lnk_cityChooserPopup_kiev;

    @FindBy(xpath = "//*[@locality_id='30'][contains(text(), 'Одесса')]")
    public WebElement lnk_cityChooserPopup_odessa;

    @FindBy(xpath = "//*[@locality_id='31'][contains(text(), 'Харьков')]")
    public WebElement lnk_cityChooserPopup_kharkov;

}
