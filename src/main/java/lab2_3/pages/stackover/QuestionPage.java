package lab2_3.pages.stackover;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by juice on 29.05.2016.
 */
public class QuestionPage {
    private WebDriver driver;

    public QuestionPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "//*[@id='qinfo']/tbody/tr[1]/td[2]/p")
    public WebElement elem_questionPage_today;
}
